-- MySQL dump 10.13  Distrib 8.0.33, for macos13.3 (x86_64)
--
-- Host: localhost    Database: example_app
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  `seo_title` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(160) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_categories`
--

LOCK TABLES `blog_categories` WRITE;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
INSERT INTO `blog_categories` VALUES (2,'Tin nhanh','tin-nhanh','mé nó chứ\n',1,NULL,NULL,'2023-07-24 00:05:51','2023-07-31 02:49:52');
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `blog_posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `blog_author_id` bigint unsigned DEFAULT NULL,
  `blog_category_id` bigint unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_at` date DEFAULT NULL,
  `seo_title` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` varchar(160) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts`
--

LOCK TABLES `blog_posts` WRITE;
/*!40000 ALTER TABLE `blog_posts` DISABLE KEYS */;
INSERT INTO `blog_posts` VALUES (2,2,2,'Hashing password fields and handling password updates','hashing-password-fields-and-handling-password-updates','<h2>Headding</h2><p><figure data-trix-attachment=\"{&quot;contentType&quot;:&quot;image/png&quot;,&quot;filename&quot;:&quot;LOGO-NEWS-716X144-Edited-BACKGROUND.png&quot;,&quot;filesize&quot;:65089,&quot;height&quot;:144,&quot;href&quot;:&quot;http://localhost:8000/storage/blog-posts/content/ZMqhx0C7wnY5qMHPSwRHZVkgEEVVP96hoj7wOEzo.png&quot;,&quot;url&quot;:&quot;http://localhost:8000/storage/blog-posts/content/ZMqhx0C7wnY5qMHPSwRHZVkgEEVVP96hoj7wOEzo.png&quot;,&quot;width&quot;:716}\" data-trix-content-type=\"image/png\" data-trix-attributes=\"{&quot;caption&quot;:&quot;aaaa&quot;,&quot;presentation&quot;:&quot;gallery&quot;}\" class=\"attachment attachment--preview attachment--png\"><a href=\"http://localhost:8000/storage/blog-posts/content/ZMqhx0C7wnY5qMHPSwRHZVkgEEVVP96hoj7wOEzo.png\"><img src=\"http://localhost:8000/storage/blog-posts/content/ZMqhx0C7wnY5qMHPSwRHZVkgEEVVP96hoj7wOEzo.png\" width=\"716\" height=\"144\"><figcaption class=\"attachment__caption attachment__caption--edited\">aaaa</figcaption></a></figure></p>','2023-07-01',NULL,'asdasd','blog-posts/thumbnail/Screenshot 2023-07-13 at 09.02.19.png','2023-07-22 10:03:33','2023-07-24 00:06:49');
/*!40000 ALTER TABLE `blog_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(9,'2023_07_22_092342_create_blog_table',3),(17,'2023_07_31_073841_create_tags_table',5),(18,'2023_07_22_071212_create_permission_tables',6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',2),(2,'App\\Models\\User',2);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'view_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(2,'view_any_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(3,'create_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(4,'update_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(5,'restore_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(6,'restore_any_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(7,'replicate_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(8,'reorder_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(9,'delete_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(10,'delete_any_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(11,'force_delete_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(12,'force_delete_any_category','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(13,'view_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(14,'view_any_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(15,'create_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(16,'update_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(17,'restore_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(18,'restore_any_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(19,'replicate_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(20,'reorder_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(21,'delete_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(22,'delete_any_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(23,'force_delete_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(24,'force_delete_any_permission','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(25,'view_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(26,'view_any_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(27,'create_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(28,'update_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(29,'restore_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(30,'restore_any_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(31,'replicate_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(32,'reorder_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(33,'delete_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(34,'delete_any_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(35,'force_delete_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(36,'force_delete_any_post','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(37,'view_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(38,'view_any_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(39,'create_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(40,'update_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(41,'delete_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(42,'delete_any_role','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(43,'view_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(44,'view_any_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(45,'create_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(46,'update_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(47,'restore_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(48,'restore_any_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(49,'replicate_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(50,'reorder_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(51,'delete_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(52,'delete_any_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(53,'force_delete_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(54,'force_delete_any_tag','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(55,'view_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(56,'view_any_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(57,'create_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(58,'update_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(59,'restore_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(60,'restore_any_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(61,'replicate_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(62,'reorder_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(63,'delete_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(64,'delete_any_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(65,'force_delete_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(66,'force_delete_any_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(67,'view_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(68,'view_any_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(69,'create_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(70,'update_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(71,'restore_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(72,'restore_any_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(73,'replicate_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(74,'reorder_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(75,'delete_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(76,'delete_any_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(77,'force_delete_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(78,'force_delete_any_website','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(79,'page_Profile','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(80,'widget_UserOverview','web','2023-08-01 02:21:17','2023-08-01 02:21:17');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super_admin','web','2023-08-01 02:21:17','2023-08-01 02:21:17'),(2,'filament_user','web','2023-08-01 02:21:17','2023-08-01 02:21:17');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Admin','admin','2023-08-01 01:58:21','2023-08-01 01:58:21');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Admin','supper@admin.com',NULL,'$2y$10$T1CnUcjnJM/oLyslKcmu7uoVMeeJo/PegrERB9oYB6f3u/0yHDJz.','wkaAS0lvokc0rUg4ds32HPrjbtZxIFbU974REOci9eThfDnJCRfH9YfYNlXO','2023-07-22 00:49:36','2023-07-22 00:51:37'),(3,'Hoài Ân','tahoaiantkw@gmail.com',NULL,'$2y$10$lh0XNXvOeLpfoBTPRT7zaeBFKHW4PtuDpPxfBXiHNhgYgqrWc3/qC',NULL,'2023-07-22 00:52:54','2023-07-23 19:52:03'),(4,'Mon','mon@gmail.com',NULL,'$2y$10$gWkm.gez26W8IIvtEPizA.cZ2G0lGtmFIpgd.pOvwJNNBhZmkhzWK',NULL,'2023-10-08 20:57:11','2023-10-08 20:57:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `websites`
--

DROP TABLE IF EXISTS `websites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `websites` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `websites`
--

LOCK TABLES `websites` WRITE;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;
INSERT INTO `websites` VALUES (1,'website/LOGO-NEWS-716X144-Edited-BACKGROUND.png','Kênh 14','https://kenh14.vn','2023-08-01 01:29:45','2023-08-01 01:29:45');
/*!40000 ALTER TABLE `websites` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-09 11:11:21
